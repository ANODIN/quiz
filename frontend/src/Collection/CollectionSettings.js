import React, {useState} from "react";
import PropType from "prop-types";

import '../css/style.css';
import './Collection.css';

function CollectionSettings({id, onSubmit, data, submitName }) {

    const [displayNameValue, setDisplayName] = useState((data && data.displayName) || '');
    const [descriptionValue, setDescription] = useState((data && data.description) || '');

    const handlerOnSubmit = e => {
        const data = {
            displayName: displayNameValue,
            description: descriptionValue,
        }
        onSubmit(e, data, id);
    }

    return (<div>
            <form onSubmit={e => handlerOnSubmit(e)}>
                <div>
                    <label htmlFor="txt_name">Nom de la collection</label>
                    <input onChange={({target: {value}}) => setDisplayName(value)} value={displayNameValue} type="text" name="displayName" id="txt_name" required/>
                </div>

                <div>
                    <label htmlFor="txt_desc">Description</label>
                    <input onChange={({target: {value}}) => setDescription(value)} value={descriptionValue} type="textarea" name="description" id="txt_desc"/>
                </div>

                <input type="submit" value={submitName}/>
            </form>
        </div>
    )
}

CollectionSettings.propTypes = {
    id: PropType.number.isRequired,
    onSubmit: PropType.func.isRequired
}

export default CollectionSettings;
