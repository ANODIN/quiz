import React, {Component, useState} from "react";

import '../Question/Question.css';
import '../css/style.css';
import CollectionSettings from "./CollectionSettings";

const MAX_QUESTION = 5;

function QuestionEditor({id, question: {answers, imageURL, multipleChoice, question}, onSubmit}) {

    const [answersValue, setAnswers] = useState(answers);
    const [multipleChoiceValue, setMultipleChoice] = useState(multipleChoice);
    const [questionValue, setQuestion] = useState(question);

    const handleAnswersChange = ({target: {value}}, id) => {
        setAnswers([...answersValue.map((v, k) => (k === id) ? value : v)]);
    }

    const handleAddAnswer = (e) => {
        e.preventDefault();
        if (answersValue.length < MAX_QUESTION)
            setAnswers([...answersValue, "nouvelle réponses"]);
    }

    const handleDeleteAnswer = (e, id) => {
        e.preventDefault();
        setAnswers([...answersValue.filter((v, k) => k !== id)]);
    }

    const handleOnSubmit = (e, id) => {
        e.preventDefault();
        const data = {
            answers: answersValue,
            multipleChoice: multipleChoiceValue,
            question: questionValue
        };
        onSubmit(e, id, data)
    }

    return (<form onSubmit={e => handleOnSubmit(e, id)} className="question-container">
        <div className="letf-ctn">
            <div className="img">
                <img src={imageURL} alt=""/>
            </div>
            <input type="file" name="image" id=""/>
        </div>
        <div className="right-ctn">
            <div className="title">
                <label htmlFor="txt_title">Titre</label>
                <input
                    value={questionValue}
                    onChange={({target: {value}}) => setQuestion(value)}
                    type="text"
                    name=""
                    id="txt_title"/>
            </div>
            <div className="nav">
                <label htmlFor="multiple_choice">Choix multiple</label>
                <input
                    value={multipleChoiceValue}
                    onChange={({target: {checked}}) => setMultipleChoice(checked)}
                    type="checkbox"
                    name="multipleChoice"
                    id="multiple_choice"/>
                <a href="#" onClick={e => handleAddAnswer(e)}>Ajouter une question</a>
                <input type="submit" value="Sauvegarder"/>
            </div>
            <div>

                {/*QUESTION LIST*/}
            </div>
            <div className="answers">
                <ul className="question-list">
                    {answersValue.map((v, k) => (
                        <li key={k} className="answer-item input">
                            <input onChange={e => handleAnswersChange(e, k)} value={v} type="text" name="" id=""/>
                            <a className="" href="" onClick={e => handleDeleteAnswer(e, k)}>Suprimer</a>
                        </li>
                    ))}
                </ul>
            </div>
            {/*QUESTION LIST*/}
            <div>
            </div>
        </div>
    </form>)
}

class CollectionEditor extends Component{

    constructor(props) {
        super(props);

        this.state = {
            collection: props.collection || {
                _id: undefined,
                questions: undefined
            }
        };
    }

    _handleModifyCollection = (e, data, collectionId) => {
        e.preventDefault();
    }

    _handleCreateCollection = (e, data) => {
        e.preventDefault();

        fetch('http://localhost:3000/api/collection', {
            method: 'POST',
            //mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(data => console.log(data));
    }

    _handleOnValueChange = ({target}, id) => {
        console.log(target.type);
    }

    _handleOnQuestionSubmit = (e, questionId, data) => {
        e.preventDefault();
        console.log(questionId, data);
    }

    render ()
    {
        const { collection } = this.state;

        return(<div className="ctn-lg">
                <CollectionSettings
                    onSubmit={(collection._id !== undefined) ? this._handleModifyCollection : this._handleCreateCollection}
                    submitName={(collection._id !== undefined) ? 'Sauvegarder' : 'Crée'}
                    data={collection}
                    id={collection._id}/>
            <div>
                <div>{collection.questions && collection.questions.map((v, k) =>(<QuestionEditor
                    key={k}
                    id={k}
                    onSubmit={this._handleOnQuestionSubmit}
                    onChange={this._handleOnValueChange}
                    question={v}/>))
                }</div>
                <div>
                    <a href="">Ajouter une question</a>
                </div>
            </div>
        </div>);
    }
}

export default CollectionEditor;
