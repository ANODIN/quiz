import React, {Component, useState} from "react";
import CollectionEditor from "./CollectionEditor";

import '../css/style.css';
import './Collection.css';

function Collection({id, name, description, questionNumber, onEdit, onDelet}) {
    return(
        <div className="ctn" id={id}>
            <h1>{name}</h1>
            <p>{(description) ? description : 'Aucune description disponible.'}</p>

            <div className="flat-footer">
                <div className="left-ctn">
                    <p className="ctn">{(questionNumber > 0) ? `${questionNumber} questions` : 'aucun question'}</p>
                </div>
                <div className="right-ctn items">
                    <ul>
                        <li className="item"><a href="" onClick={(e) => onEdit(e, id)}>édité</a></li>
                        <li className="item"><a href="" onClick={(e) => onDelet(e, id)}>suprimer</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

class CollectionList extends Component{

    state = {
        isLoading: true,
        collections: [],
        //msg: undefined,
        currentCollection: null
    }

    constructor(props) {
        super(props);

        this._onLoaddingCollections();
    }

    _onLoaddingCollections = () => {

        fetch('http://localhost:3000/api/collection')
            .then(res => res.json())
            .then(data => this.setState({
                isLoading: false,
                collections: data
            }))
            .catch(e => console.log(e));
    }

    _handleOnEdit = (e, id) => {
        e.preventDefault();

        const collection = this.state.collections[id];

        if(collection)
        {
            this.setState({ currentCollection: collection })
        }
    }

    _handleOnDelete = (e, id) => {
        e.preventDefault();
    }

    _displayList = (isLoading, collections) =>
        (isLoading)
            ? <p>loading</p>
            : collections.map((v, k) => <Collection
                key={k}
                id={k}
                name={v.displayName}
                description={v.description}
                onEdit={this._handleOnEdit}
                onDelet={this._handleOnDelete}
                questionNumber={(v.questions && v.questions.length) || 0}/>)

    render ()
    {
        const { isLoading, collections, currentCollection } = this.state;

        return(<div className="ctn-lg">
            {(currentCollection === null)
                ? this._displayList(isLoading, collections)
                : <CollectionEditor collection={currentCollection} />}
        </div>);
    }
}


export default CollectionList;
