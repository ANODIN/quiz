import PropType from "prop-types"
import React, {Component} from "react"

import './Question.css'
import Question from "./Question";

class QuestionSession extends Component {

    constructor(props) {
        super(props);

        const { answers } = props.data
        const data = answers || []

        this.state = {
            historyAnswer: [ new Set() ],
            currentIndex: 0,
            maxIndex: data.length,
            data: data
        }
    }

    _handleNextQuestion = (answers) => {
        this.setState(s => {
            let newIndex = s.currentIndex + 1

            if(newIndex == s.maxIndex)
                this._handleSessionOver(s)
            else {
                return {
                    currentIndex: newIndex,
                    historyAnswer: [].concat(s.historyAnswer, new Set())
                }
            }

            return {}
        })
    }

    _handleSessionOver = ({historyAnswer}) => {
        const {onSessionOver} = this.props
        onSessionOver(historyAnswer)
    }

    _handleOnSelect = (e, i) => {
        const { data } = this.props
        const { historyAnswer, currentIndex } = this.state

        const {multipleChoice} = data.answers[currentIndex]
        const currentAnswersSet = historyAnswer.splice(currentIndex, 1)[0]

        if(!currentAnswersSet.has(i))
        {
            if(multipleChoice)
            {
                let newIndex = currentAnswersSet.add(i)
                this.setState({
                    historyAnswer: [].concat(historyAnswer, currentAnswersSet)
                })
            }
            else
            {
                this.setState({
                    historyAnswer: [].concat(historyAnswer, new Set([i]))
                })
            }
        }
        else
        {
            this.setState({
                historyAnswer: [].concat(
                    historyAnswer,
                    (currentAnswersSet.delete(i)) && currentAnswersSet)
            })
        }
    }

    render() {
        const {currentIndex, maxIndex, data, historyAnswer} = this.state
        console.log(historyAnswer)
        return (<div>
            <div className="question-header">
                <h3>{currentIndex + 1}/{maxIndex}</h3>
            </div>
            <Question
                items={data[currentIndex]}
                nextQuestion={this._handleNextQuestion}
                answersSet={historyAnswer[currentIndex]}
                handleOnSelect={this._handleOnSelect}
            />
        </div>)
    }
}

QuestionSession.propTypes = {
    data: PropType.arrayOf({
        answers: PropType.shape({
            id: PropType.number.isRequired,
            img: PropType.string.isRequired,
            question: PropType.string.isRequired,
            answers: PropType.array.isRequired,
            multipleChoice: PropType.bool.isRequired
        })
    }),
    onSessionOver: PropType.func.isRequired
}

export default QuestionSession
