import React, {Component} from 'react';

/***MY COMPONENTS***/
import QuestionSession from "./QuestionSession";
import Results from "./Results/Results";
import CollectionEditor from "./Collection/CollectionEditor";
import CollectionList from "./Collection/CollectionList";

const API_REQUEST_QUESTION = {
    answers:[
        {
            id: 297356,
            img: '/img/tiara-aracama-2xRYFjsxgvc-unsplash.jpg',
            question : 'Boire la "rak" (comme dit les créoles) et incompatible avec la conduit ?',
            answers : [
                "Si je suis à plus de 0,50g/l de sang",
                "Si je suis à plus de 0,10mg/l d'air",
                "Si je met la radio à sont maximun",
            ],
            multipleChoice: true
        },
        {
            id: 29787892,
            img: '/img/ahmed-zayan-1svWCsnnrT8-unsplash.jpg',
            question : 'Consommer du canabis nuie à la conduit ?',
            answers : [
                "oui",
                "non"
            ],
            multipleChoice: false
        },
        {
            id: 378354325,
            img: '/img/michael-jin-ipHlSSaC3vk-unsplash.jpg',
            question : 'Qu\'elle sont les facteurs d\'accident ?',
            answers : [
                "l'alcool",
                "les autres usagers",
                "le cannabis",
                "la radio"
            ],
            multipleChoice: true
        }
    ]
}

const API_REQUEST_ANSWERS = {
    answers:[
        {
            id: 297356,
            answer: [0, 1]
        },
        {
            id: 29787892,
            answer: [0]
        },
        {
            id: 378354325,
            answer: [0, 2]
        }
    ]
}

class App extends Component {

    state = {
        isOver: false,
        historyAnswer: []
    }

    _handleSessionOver = (h) =>{
        const { isOver } = this.state
        this.setState({
            isOver: true,
            historyAnswer: h})
    }

    _getPage = (index) => {
        switch(index){
            case 0:
            case 2:
                return <CollectionList />
            case 3:
                return <CollectionEditor />
        }
    }

    //TODO: Show the correct answer
    render() {
        const {isOver, historyAnswer} = this.state
        return (<div>
            {this._getPage(2)}
            {/*{isOver ?*/}
            {/*    <Results*/}
            {/*        historyAnswer={historyAnswer}*/}
            {/*        data={API_REQUEST_QUESTION}*/}
            {/*        answerData={API_REQUEST_ANSWERS}/> :*/}
            {/*    <QuestionSession*/}
            {/*        data={API_REQUEST_QUESTION}*/}
            {/*        onSessionOver={this._handleSessionOver}/>*/}
            {/*}*/}
        </div>)
    }
}

export default App;
