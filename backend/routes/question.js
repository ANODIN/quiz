const express = require('express');
const router = express.Router();

const multer = require('../middleware/multer-config');
const questionCtrl = require('../controllers/question');

router.get('/', questionCtrl.getAll);
router.get('/:id', questionCtrl.getOne);
//router.get('/:id/answers', questionCtrl.getAnswers)

//Editing
router.post('/', multer, questionCtrl.createOne);
router.put('/:id', multer, questionCtrl.modifyOne)

module.exports = router;
