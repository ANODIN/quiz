const controllers = require('./controllers');
const mongoose = require('mongoose');

const Question = require('../models/Question');
const Collection = require('../models/Collection');

exports.getAll = (req, res, next) => {
    Question.find()
        .then(question => res.status(200).json(question))
        .catch(e => res.status(400).json({ e }));
};

exports.getOne = (req, res, next) => {
    Question.findOne({ _id: req.params.id})
        .then(question => {
            //delete question.rightAnswer;
            return res.status(200).json(question);
        })
        .catch(e => res.status(404).json({ e }));
};

exports.createOne = (req, res, next) => {
    Collection.findOne({ _id: req.params.id })
        .then(col => {
            const questionObject = JSON.parse(req.body.question);
            delete questionObject._id;

            const qModel = mongoose.model('Question', Question)

            const newQuestion = new qModel({
                ...questionObject,
                imageURL: `${req.protocol}://${req.get('host')}/public/images/${req.file.filename}`
            });

            const collectionObject = (col.questions === undefined)
                ? { questions: [newQuestion] }
                : { questions: [...col.questions, newQuestion] };

            console.log(collectionObject);

            Collection.updateOne({ _id: req.params.id }, {...collectionObject, _id: req.params.id})
                .then(() => res.status(201).json({ msg: 'Object created'}))
                .catch(e => controllers.badRequest(e, res));
        })
        .catch(e => controllers.notFound(e, res));
};

///TODO: Remove older images
exports.modifyOne = (req, res, next) => {

    const lastQuestionObject = JSON.parse(req.body.question);
    const questionObject = (req.file)
        ? { ...lastQuestionObject, imageURL: `${req.protocol}://${req.get('host')}/public/images/${req.file.filename}`}
        : { ...lastQuestionObject };

    Question.updateOne({ _id: req.params.id }, { ...questionObject, _id: req.params.id})
        .then(() => res.status(200).json({ msg: 'modifié !'}))
        .catch(e => res.status(400).json({ e }));
};

exports.getAnswers = (req, res, next) => {
    Question.findOne({ _id: req.params.id})
        .then(question => res.status(200).json({ rightAnswer: question.rightAnswer }))
        .catch(e => res.status(404).json({ e }));
};
