const mongoose = require('mongoose');

const questionSchema = mongoose.Schema({
    type: {type: Number, required: true},
    question: {type: String, required: true},
    answers: [String],
    imageURL: { type: String, required: true },
    multipleChoice: { type: Boolean, required: true }
});

//module.exports = mongoose.model('Question', questionSchema);
module.exports = questionSchema;
